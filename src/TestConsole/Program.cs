﻿// See https://aka.ms/new-console-template for more information

using System.Text.RegularExpressions;

var regExp = @"^[a-zA-ZА-Яа-я]+$";

var testString = "Петр";

var matchCase = new Regex( regExp );

if ( matchCase.IsMatch( testString ) )
    Console.WriteLine( "matched" );
