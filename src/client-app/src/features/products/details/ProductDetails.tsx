import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Card, Image } from 'semantic-ui-react';
import LoadingComponent from '../../../app/layout/LoadingComponent';
import { useStore } from '../../../app/stores/store';

export default observer(function ProductDetails() {
    const {productStore} = useStore();
    const {selectedProduct: product, loadProduct, loadingInitial} = productStore;
    const {id} = useParams<{id: string}>();

    useEffect(() => {
        if (id) loadProduct(id);
    }, [id, loadProduct]);

    if (loadingInitial || !product) return <LoadingComponent />;

    return (        
        <Card fluid className='product-details'>
            <Card.Content>
                <Card.Description as='h1'>{product.name}</Card.Description>
                <Card.Meta><span>{product.category}</span></Card.Meta>
                <div style={{display: 'flex', justifyContent: 'center'}}><Image src={`${product.image}`} /></div>
                <Card.Description as='h4'>
                    <div>Цена: {product.price} руб</div>
                    <div>Количество: {product.quantity}</div>
                </Card.Description>
                <Card.Description as='h2'>Описание</Card.Description>
                <br/>
                <Card.Description className='product-description'>
                    <div dangerouslySetInnerHTML={{__html: product.description}}></div>
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Button.Group widths='2'>
                    <Button as={Link} to={`/manage/${product.id}`} basic color='blue' content='Редактировать' />
                    <Button as={Link} to='/products' basic color='grey' content='Закрыть' />
                </Button.Group>
            </Card.Content>
        </Card>        
    )
})