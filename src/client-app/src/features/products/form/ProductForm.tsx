import { observer } from 'mobx-react-lite';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Card, Form, Segment, Image } from 'semantic-ui-react';
import LoadingComponent from '../../../app/layout/LoadingComponent';
import { useStore } from '../../../app/stores/store';
import { v4 as uuid } from 'uuid';

export default observer(function ProductForm() {
    const navigate = useNavigate();
    const {productStore} = useStore();
    const {createProduct, updateProduct, 
            loading, loadProduct, loadingInitial} = productStore;
    const {id} = useParams<{id: string}>();

    const [product, setProduct] = useState({
        id: '',
        name: '',
        category: '',
        description: '',
        image: '',
        price: 0,
        quantity: 0
    });

    useEffect(() => {
        if (id) loadProduct(id).then(product => setProduct(product!))
    }, [id, loadProduct]);

    function handleSubmit() {
        if (product.id.length === 0) {
            let newProduct = {
                ...product,
                id: uuid()
            };
            createProduct(newProduct).then(() => navigate(`/products/${newProduct.id}`))
        } else {
            updateProduct(product).then(() => navigate(`/products/${product.id}`))
        }
    }

    function handleInputChange(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
        const {name, value} = event.target;
        setProduct({...product, [name]: value})
    }

    if (loadingInitial) return <LoadingComponent content='Загрузка...' />

    return (
        <>
            <Segment clearing>
                <Form onSubmit={handleSubmit} autoComplete='off'>
                    <Form.Input placeholder='Название' value={product.name} name='name' onChange={handleInputChange} />
                    <Form.TextArea placeholder='Описание' value={product.description} name='description' onChange={handleInputChange} style={{minHeight: 160}} />
                    <Form.Input placeholder='Категория' value={product.category} name='category' onChange={handleInputChange} />
                    <Form.Input placeholder='Изображение' value={product.image} name='image' onChange={handleInputChange} />
                    <Form.Input placeholder='Цена' value={product.price} name='price' onChange={handleInputChange} />
                    <Form.Input placeholder='Количество' value={product.quantity} name='quantity' onChange={handleInputChange} />
                    <Button.Group widths='2'>
                        <Button as={Link} to='/products' basic color='grey' content='Закрыть' />
                        <Button loading={loading}  basic color='green' content='Сохранить' />
                    </Button.Group>
                </Form>
            </Segment>
            <br/>
            <Card fluid className='product-details'>
                <Card.Content>
                    <Card.Header>{product.name}</Card.Header>
                    <Card.Meta><span>{product.category}</span></Card.Meta>
                    <div style={{display: 'flex', justifyContent: 'center'}}><Image src={`${product.image}`} /></div>                
                    <Card.Description>
                        <div dangerouslySetInnerHTML={{__html: product.description}}></div>
                    </Card.Description>
                </Card.Content>
            </Card>            
        </>
    )
})