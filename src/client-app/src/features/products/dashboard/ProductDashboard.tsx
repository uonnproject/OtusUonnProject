import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { Grid } from 'semantic-ui-react';
import LoadingComponent from '../../../app/layout/LoadingComponent';
import { useStore } from '../../../app/stores/store';
import ProductList from './ProductList';

export default observer(function ProductDashboard() {
    const {productStore} = useStore();
    const {loadProducts, productRegistry} = productStore;

    useEffect(() => {
      if (productRegistry.size <= 1) loadProducts();
    }, [productRegistry.size, loadProducts])
  
    if (productStore.loadingInitial) return <LoadingComponent content='Загрузка...' />

    return (
        <Grid>
            <Grid.Column width='16'>
                <ProductList />
            </Grid.Column>
        </Grid>
    )
})