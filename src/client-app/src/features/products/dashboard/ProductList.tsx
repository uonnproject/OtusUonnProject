import { observer } from 'mobx-react-lite';
import React, { SyntheticEvent, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, Image, Segment } from 'semantic-ui-react';
import { useStore } from '../../../app/stores/store';

export default observer(function ProductList() {
    const {productStore} = useStore();
    const {deleteProduct, productsList, loading} = productStore;

    const [target, setTarget] = useState('');

    function handleProductDelete(e: SyntheticEvent<HTMLButtonElement>, id: string) {
        setTarget(e.currentTarget.name);
        deleteProduct(id);
    }

    return (
        <Card.Group className='product-list-card'>
            {productsList.map(product => (
                <Card>            
                    <Card.Content>
                        <Image src={`${product.image}`} />
                    </Card.Content>
                    <Card.Content>
                        <Card.Header>{product.name}</Card.Header>
                        <Card.Meta>
                            <span>{product.category}</span>
                        </Card.Meta>
                        <Card.Description as='h4'>
                            <div>Цена: {product.price} руб</div>
                            <div>Количество: {product.quantity}</div>
                        </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                        <Button.Group widths='2'>
                            <Button as={Link} to={`/products/${product.id}`} basic color='blue' content='Открыть' />
                            <Button 
                                    name={product.id}
                                    loading={loading && target === product.id} 
                                    onClick={(e) => handleProductDelete(e, product.id)} 
                                    basic                                    
                                    color='red'
                                    content='Удалить'
                                />
                        </Button.Group>
                    </Card.Content>
                </Card>
            ))}
        </Card.Group>
    )
})