export interface Manager {
    id: string
    firstName: string
    surname: string
    fullName: string
    email: string
}