import React from 'react';
import { Container } from 'semantic-ui-react';
import NavBar from './NavBar';
import ProductDashboard from '../../features/products/dashboard/ProductDashboard';
import { observer } from 'mobx-react-lite';
import { Route, Routes, useLocation } from 'react-router-dom';
import ProductForm from '../../features/products/form/ProductForm';
import ProductDetails from '../../features/products/details/ProductDetails';

function App() {
  const location = useLocation();

  return (
    <>
        <NavBar />
        <Container className='main-container'>
            <Routes>                
                <Route index element={<ProductDashboard />} />
                <Route path='products' element={<ProductDashboard />} /> 
                <Route path='products/:id' element={<ProductDetails />} />
                <Route key={location.key} path={'/createProduct'} element={<ProductForm />} />
                <Route key={location.key} path={'createProduct/:id'} element={<ProductForm />} />
                <Route key={location.key} path={'/manage/:id'} element={<ProductForm />} />
            </Routes>
        </Container>
        <br/>
        <br/>
    </>
  );
}

export default observer(App);
