using DataAccess;
using Domain.Entities;

namespace UnitTests.Helpers.Seeders;

public static class ManagerSeeder
{
    public static void Seed(
        DataContext dataContext,
        bool clearBeforeSeed = true )
    {
        var newManagers = new List<Manager>( );

        newManagers.AddRange(
            new List<Manager>
            {
                new( )
                {
                    Email = "ivanov@mail.ru",
                    FirstName = "Ivan",
                    LastName = "Ivanov",
                    Phone = "89094578934",
                    BirthDate = new DateTime( 1980, 5, 24 )
                },
                new( )
                {
                    Email = "smirnov@gmail.ru",
                    FirstName = "Sergey",
                    LastName = "Smirnov",
                    Phone = "89094586945",
                    BirthDate = new DateTime( 1988, 7, 14 )
                },
                new( )
                {
                    Email = "fedotov@mail.ru",
                    FirstName = "Alexey",
                    LastName = "Fedotov",
                    Phone = "89092378934",
                    BirthDate = new DateTime( 1986, 8, 20 )
                },
                new( )
                {
                    Email = "dudov@mail.ru",
                    FirstName = "Mikhail",
                    LastName = "Dudov",
                    Phone = "89052578935",
                    BirthDate = new DateTime( 1988, 7, 15 )
                }
            }
        );

        if ( clearBeforeSeed )
        {
            dataContext.Managers.RemoveRange( dataContext.Managers );
        }

        dataContext.Managers.AddRange( newManagers );
        dataContext.SaveChanges( );
    }
}