using DataAccess;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace UnitTests.IntegrationTests;

public abstract class BaseIntegrationTest : IClassFixture<IntegrationTestWebApplicationFactory>
{
    private readonly IServiceScope _scope;
    protected readonly ISender Sender;
    protected readonly DataContext DataContext;
    
    protected BaseIntegrationTest( IntegrationTestWebApplicationFactory factory )
    {
        _scope = factory.Services.CreateScope( );

        Sender = _scope.ServiceProvider.GetRequiredService<ISender>( );
        DataContext = _scope.ServiceProvider.GetRequiredService<DataContext>( );
    }
}