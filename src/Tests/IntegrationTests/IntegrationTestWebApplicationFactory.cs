using API;
using DataAccess;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Testcontainers.MsSql;

namespace UnitTests.IntegrationTests;

public class IntegrationTestWebApplicationFactory
    : WebApplicationFactory<Program>, IAsyncLifetime
{
    private readonly MsSqlContainer _msSqlContainer = new MsSqlBuilder( )
        .WithImage( "mcr.microsoft.com/mssql/server:2019-latest" )
        .Build( );

    protected override void ConfigureWebHost( IWebHostBuilder builder )
    {
        builder.ConfigureTestServices( services =>
        {
            var descriptor = services.SingleOrDefault( s => s.ServiceType == typeof( DbContextOptions<DataContext> ) );

            if ( descriptor != null )
                services.Remove( descriptor );
            services.AddDbContext<DataContext>( options =>
            {
                options.UseSqlServer( _msSqlContainer.GetConnectionString( ) );
            } );
        } );
    }

    public Task InitializeAsync( )
    {
        return _msSqlContainer.StartAsync( );
    }

    public new Task DisposeAsync( )
    {
        return _msSqlContainer.StopAsync( );
    }
}