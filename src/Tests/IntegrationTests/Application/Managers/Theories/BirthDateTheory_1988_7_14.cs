namespace UnitTests.IntegrationTests.Application.Managers.Theories;

public class BirthDateTheory_1988_7_14 : TheoryData<DateTime>
{
    public BirthDateTheory_1988_7_14( )
    {
        Add(new DateTime(1988, 7, 14));
    }
}