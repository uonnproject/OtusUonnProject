namespace UnitTests.IntegrationTests.Application.Managers.Theories;

public class BirthDateTheory_1988_7_14_To_1988_7_15 : TheoryData<DateTime, DateTime>
{
    public BirthDateTheory_1988_7_14_To_1988_7_15( )
    {
        Add( new DateTime( 1988, 7, 14 ), new DateTime( 1988, 7, 15 ) );
    }
}