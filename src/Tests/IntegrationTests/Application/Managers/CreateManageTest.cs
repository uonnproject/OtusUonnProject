using Application.Managers.Commands.CreateManager;
using Application.Exceptions;
using Application.Managers.Response;
using Domain.Entities;
using Shouldly;

namespace UnitTests.IntegrationTests.Application.Managers;

public class CreateManageTest
    : BaseIntegrationTest
{
    public CreateManageTest( IntegrationTestWebApplicationFactory factory )
        : base( factory )
    {
    }

    [Fact]
    public async Task CreateNewManager_ValidData_SavesToDb( )
    {
        // Arrange
        var command = new CreateManagerCommand
        {
            FirstName = "Петр",
            LastName = "Михайлович",
            Email = "petrm@ya.ru",
            BirthDate = new DateTime( 1989, 2, 25 ),
            Phone = "89032351234"
        };

        // Act
        var result = await Sender.Send( command );
        // Assert
        result.ShouldBeOfType<ManagerResponse>( );
        var managerInDb = DataContext.Set<Manager>( ).FirstOrDefault( x => x.Id == result.Id );
        managerInDb.ShouldNotBeNull();
        managerInDb.Id.ShouldNotBe( Guid.Empty );
    }

    [Fact]
    public async Task CreateNewManager_InvalidEmail_ThrowValidationException( )
    {
        // Arrange
        var command = new CreateManagerCommand
        {
            FirstName = "Петр",
            LastName = "Михайлович",
            Email = "123",
            BirthDate = new DateTime( 1989, 2, 25 ),
            Phone = "89032351234"
        };

        // Act
        Task Action( ) => Sender.Send( command );

        // Assert
        await Assert.ThrowsAsync<ApplicationValidationException>( Action );
    }

    [Fact]
    public async Task CreateNewManager_InvalidFirstName_ThrowValidationException( )
    {
        // Arrange
        var command = new CreateManagerCommand
        {
            FirstName = "Петр123",
            LastName = "Михайлович",
            Email = "petrm@ya.ru",
            BirthDate = new DateTime( 1989, 2, 25 ),
            Phone = "89032351234"
        };

        // Act
        Task Action( ) => Sender.Send( command );

        // Assert
        await Assert.ThrowsAsync<ApplicationValidationException>( Action );
    }

    [Fact]
    public async Task CreateNewManager_InvalidLastName_ThrowValidationException( )
    {
        // Arrange
        var command = new CreateManagerCommand
        {
            FirstName = "Петр",
            LastName = "Михайлович123",
            Email = "petrm@ya.ru",
            BirthDate = new DateTime( 1989, 2, 25 ),
            Phone = "89032351234"
        };

        // Act
        Task Action( ) => Sender.Send( command );

        // Assert
        await Assert.ThrowsAsync<ApplicationValidationException>( Action );
    }
}