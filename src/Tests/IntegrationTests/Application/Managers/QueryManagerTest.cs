using Application.Abstractions;
using Application.Managers.Queries.GetAllManagers;
using Application.Managers.Queries.GetManagerByFilter;
using Application.Managers.Response;
using DataAccess.Abstractions.Managers;
using DataAccess.Abstractions.Pagination;
using Shouldly;
using UnitTests.Helpers.Seeders;
using UnitTests.IntegrationTests.Application.Managers.Theories;

namespace UnitTests.IntegrationTests.Application.Managers;

public class QueryManagerTest
    : BaseIntegrationTest
{
    public QueryManagerTest( IntegrationTestWebApplicationFactory factory )
        : base( factory )
    {
        ManagerSeeder.Seed( DataContext );
    }

    [Fact]
    public async Task GetAll_ReturnsAllRecords( )
    {
        // Arrange
        var command = new GetAllManagersQuery(
            new SortPageOptions
            {
                Pagination = new Pagination
                {
                    PageNumber = 1,
                    PageSize = 10
                }
            } );

        // Act
        var result = await Sender.Send( command );

        // Assert
        result.Items.ShouldNotBeNull( );
        result.Items.Count( ).ShouldBeGreaterThanOrEqualTo( 1 );
    }

    [Theory]
    [InlineData( "Ivan" )]
    public async Task Query_FilterByName_ReturnsOneRecord( string firstName )
    {
        // Arrange
        var command = new GetManagersByFilterQuery(
            new SortFilterPageOptions
            {
                Pagination = new Pagination
                {
                    PageNumber = 1,
                    PageSize = 10
                },
                OrderOptions = OrderByOptions.SimpleOrder,
                Filter = new ManagerFilter
                {
                    FirstName = firstName
                }
            } );

        // Act
        var result = await Sender.Send( command );

        // Assert
        result.ShouldBeOfType( typeof( PagedResponse<ManagerResponse> ) );
        result.Items.Count( ).ShouldBeEquivalentTo( 1 );
        result.Items.ShouldContain( x => x.FirstName == firstName );
    }

    [Theory]
    [InlineData( "Smirnov" )]
    public async Task Query_FilterByLastName_ReturnsOneRecord( string lastName )
    {
        // Arrange
        var command = new GetManagersByFilterQuery(
            new SortFilterPageOptions
            {
                Pagination = new Pagination
                {
                    PageNumber = 1,
                    PageSize = 10
                },
                OrderOptions = OrderByOptions.SimpleOrder,
                Filter = new ManagerFilter
                {
                    LastName = lastName
                }
            } );

        // Act
        var result = await Sender.Send( command );

        // Assert
        result.ShouldBeOfType( typeof( PagedResponse<ManagerResponse> ) );
        result.Items.Count( ).ShouldBeEquivalentTo( 1 );
        result.Items.ShouldContain( x => x.LastName == lastName );
    }

    [Theory]
    [InlineData( "89094586945" )]
    public async Task Query_FilterByPhone_ReturnsOneRecord( string phone )
    {
        // Arrange
        var command = new GetManagersByFilterQuery(
            new SortFilterPageOptions
            {
                Pagination = new Pagination
                {
                    PageNumber = 1,
                    PageSize = 10
                },
                OrderOptions = OrderByOptions.SimpleOrder,
                Filter = new ManagerFilter
                {
                    Phone = phone
                }
            } );

        // Act
        var result = await Sender.Send( command );

        // Assert
        result.ShouldBeOfType( typeof( PagedResponse<ManagerResponse> ) );
        result.Items.Count( ).ShouldBeEquivalentTo( 1 );
        result.Items.ShouldContain( x => x.Phone == phone );
    }

    [Theory]
    [InlineData( "fedotov@mail.ru" )]
    public async Task Query_FilterByEmail_ReturnsOneRecord( string email )
    {
        // Arrange
        var command = new GetManagersByFilterQuery(
            new SortFilterPageOptions
            {
                Pagination = new Pagination
                {
                    PageNumber = 1,
                    PageSize = 10
                },
                OrderOptions = OrderByOptions.SimpleOrder,
                Filter = new ManagerFilter
                {
                    Email = email
                }
            } );

        // Act
        var result = await Sender.Send( command );

        // Assert
        result.ShouldBeOfType( typeof( PagedResponse<ManagerResponse> ) );
        result.Items.Count( ).ShouldBeEquivalentTo( 1 );
        result.Items.ShouldContain( x => x.Email == email );
    }

    public static TheoryData<DateTime> MemberData = new TheoryData<DateTime>
    {
        new DateTime( 1988, 7, 14 )
    };

    [Theory]
    [ClassData( typeof( BirthDateTheory_1988_7_14 ) )]
    public async Task Query_FilterByBirthDate_ReturnsOneRecord( DateTime birthDate )
    {
        // Arrange
        var command = new GetManagersByFilterQuery(
            new SortFilterPageOptions
            {
                Pagination = new Pagination
                {
                    PageNumber = 1,
                    PageSize = 10
                },
                OrderOptions = OrderByOptions.SimpleOrder,
                Filter = new ManagerFilter
                {
                    BirthDateFrom = birthDate,
                    BirthDateTo = birthDate
                }
            } );

        // Act
        var result = await Sender.Send( command );

        // Assert
        result.ShouldBeOfType( typeof( PagedResponse<ManagerResponse> ) );
        result.Items.Count( ).ShouldBeEquivalentTo( 1 );
        result.Items.ShouldContain( x => x.BirthDate == birthDate );
    }

    [Theory]
    [ClassData( typeof( BirthDateTheory_1988_7_14_To_1988_7_15 ) )]
    public async Task Query_FilterByBirthDateRange_ReturnsTwoRecords(
        DateTime birthDateFrom,
        DateTime birthDateTo )
    {
        // Arrange
        var command = new GetManagersByFilterQuery(
            new SortFilterPageOptions
            {
                Pagination = new Pagination
                {
                    PageNumber = 1,
                    PageSize = 10
                },
                OrderOptions = OrderByOptions.SimpleOrder,
                Filter = new ManagerFilter
                {
                    BirthDateFrom = birthDateFrom,
                    BirthDateTo = birthDateTo
                }
            } );

        // Act
        var result = await Sender.Send( command );

        // Assert
        result.ShouldBeOfType( typeof( PagedResponse<ManagerResponse> ) );
        result.Items.Count( ).ShouldBeEquivalentTo( 2 );
        result.Items.ShouldContain( x => x.BirthDate == birthDateFrom );
        result.Items.ShouldContain( x => x.BirthDate == birthDateTo );
    }
}