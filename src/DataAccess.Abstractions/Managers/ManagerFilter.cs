namespace DataAccess.Abstractions.Managers;

public record ManagerFilter
{
    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? Phone { get; set; }

    public string? Email { get; set; }

    public DateTime? BirthDateFrom { get; set; }

    public DateTime? BirthDateTo { get; set; }
}

