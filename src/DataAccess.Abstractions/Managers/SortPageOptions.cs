namespace DataAccess.Abstractions.Managers;

public record SortPageOptions
{
    public OrderByOptions? OrderOptions { get; set; }

    public Pagination.Pagination Pagination { get; set; } = new( );
} 