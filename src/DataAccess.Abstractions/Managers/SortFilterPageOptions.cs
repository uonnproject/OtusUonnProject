namespace DataAccess.Abstractions.Managers;

public record SortFilterPageOptions 
    : SortPageOptions
{
    public ManagerFilter? Filter { get; set; }

    public SortFilterPageOptions( )
    {
    }

    public SortFilterPageOptions(
        int pageNumber,
        int pageSize )
    {
        Pagination.PageNumber = pageNumber;
        Pagination.PageSize = pageSize;
    }
}