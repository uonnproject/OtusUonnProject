namespace DataAccess.Abstractions.Managers;

public enum OrderByOptions
{
    /// <summary>
    /// по умолчанию по ИД
    /// </summary>
    SimpleOrder = 0,
    
    /// <summary>
    /// по имени
    /// </summary>
    ByFirstName,
    
    /// <summary>
    /// по фамилии
    /// </summary>
    ByBirthDate
}