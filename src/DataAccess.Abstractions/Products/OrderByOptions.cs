namespace DataAccess.Abstractions.Products;

public enum OrderByOptions
{
    /// <summary>
    /// по умолчанию по ИД
    /// </summary>
    SimpleOrder = 0,
    
    /// <summary>
    /// по наименованию
    /// </summary>
    ByName,
    
    /// <summary>
    /// по категории
    /// </summary>
    ByCategory
}