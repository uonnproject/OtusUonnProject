namespace DataAccess.Abstractions.Pagination;

public record Pagination
{
    public const int DefaultPageSize = 20;

    public readonly int[] PageSizes =
        new[] { 5, 20, 50, 100, 500, 1000 };

    public int NumberPages { get; private set; }

    public int PageNumber
    {
        get { return _pageNumber; }
        set
        {
            // TODO move to fluent validation
            _pageNumber = value == 0
                ? throw new ArgumentException( "pageNumber can not be zero", nameof( PageNumber ) ) 
                : value;
        }
    }

    public int PageSize
    {
        get { return _pageSize; }
        set
        {
            _pageSize = PageSizes.Contains( value )
                ? value
                : DefaultPageSize;
        }
    }

    private int _pageNumber = 1;

    private int _pageSize = DefaultPageSize;

    public Pagination( )
    {
    }

    public void SetupPagination<T>( IQueryable<T> query )
    {
        NumberPages = ( int )Math.Ceiling(
            ( double )query.Count( ) / PageSize );
    }
}