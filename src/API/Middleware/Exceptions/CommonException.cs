namespace API.Middleware.Exceptions;

/// <summary>
/// Describes common application exception
/// </summary>
public sealed class CommonException
{
    /// <summary>
    /// Error`s title
    /// </summary>
    public string Title { private get; set; }

    /// <summary>
    /// Status code
    /// </summary>
    public int StatusCode { get; private set; }

    /// <summary>
    /// Message from exception.Message
    /// </summary>
    public string Message { get; private set; }

    /// <summary>
    /// List of errors
    /// </summary>
    public IReadOnlyDictionary<string, string[]>? Errors { get; private set; }

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="title">Error`s title</param>
    /// <param name="statusCode">Error`s status code</param>
    /// <param name="message">Error`s message</param>
    /// <param name="errors">Errors</param>
    public CommonException(
        string title,
        int statusCode,
        string message,
        IReadOnlyDictionary<string, string[]>? errors)
    {
        Title = title;
        StatusCode = statusCode;
        Message = message;
        Errors = errors;
    }
}