using System.Text.Json;

namespace API.Middleware.Exceptions;

/// <summary>
/// Exception handler for all kind of exceptions
/// </summary>
public class ExceptionHandlingMiddleware : IMiddleware
{
    private readonly ILogger<ExceptionHandlingMiddleware> _logger;

    /// <summary>
    /// Обработчик кастомных ошибок
    /// </summary>
    /// <param name="logger"></param>
    public ExceptionHandlingMiddleware(ILogger<ExceptionHandlingMiddleware> logger) => _logger = logger;

    /// <summary>
    /// Invoke handler
    /// </summary>
    /// <param name="context">http context</param>
    /// <param name="next">request delegate</param>
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next(context);
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);

            await HandleExceptionAsync(context, e);
        }
    }

    private static async Task HandleExceptionAsync(
        HttpContext httpContext, 
        Exception exception)
    {
        var commonException = exception.CommonExceptionGet();

        httpContext.Response.ContentType = "application/json";

        httpContext.Response.StatusCode = commonException.StatusCode;

        await httpContext.Response.WriteAsync(JsonSerializer.Serialize(commonException));
    }
}