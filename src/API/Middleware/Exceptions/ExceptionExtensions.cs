using Application.Exceptions;
using Domain.Exceptions;

namespace API.Middleware.Exceptions;

/// <summary>
/// Exception class extensions
/// </summary>
public static class ExceptionExtensions
{
    /// <summary>
    /// Get common exception
    /// </summary>
    public static CommonException CommonExceptionGet( this Exception @this )
    {
        return new CommonException(
            GetTitle( @this ),
            GetStatusCode( @this ), @this.Message,
            GetErrors( @this ) );
    }

    private static IReadOnlyDictionary<string, string[]>? GetErrors( Exception exception )
    {
        IReadOnlyDictionary<string, string[]>? errors = null;

        if ( exception is ApplicationValidationException validationException )
        {
            errors = validationException.ErrorsDictionary;
        }

        return errors;
    }

    private static int GetStatusCode( Exception exception ) =>
        exception switch
        {
            BadRequestException => StatusCodes.Status400BadRequest,
            NotFoundException => StatusCodes.Status404NotFound,
            ApplicationValidationException => StatusCodes.Status422UnprocessableEntity,
            _ => StatusCodes.Status500InternalServerError
        };

    private static string GetTitle( Exception exception ) =>
        exception switch
        {
            Domain.Exceptions.ApplicationException applicationException => applicationException.Title,
            _ => "Server Error"
        };
}