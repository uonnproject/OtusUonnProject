﻿using Application.Managers.Commands.CreateManager;
using Application.Managers.Queries.GetAllManagers;
using Application.Managers.Queries.GetManagerByFilter;
using Application.Managers.Queries.GetManagerById;
using Application.Managers.Response;
using DataAccess.Abstractions.Managers;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

/// <summary>
/// Менеджеры
/// </summary>
[ApiController]
[Route( "api/v1/[controller]" )]
public class ManagersController : BaseApiController
{
    /// <summary>
    /// Коструктор для контроллера сотрудников
    /// </summary>
    /// <param name="sender"></param>
    public ManagersController( ISender sender )
        : base( sender )
    {
    }

    /// <summary>
    /// Получить данные о сотруднике по ИД
    /// </summary>
    /// <param name="managerId">ИД сотрудника</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns></returns>
    [HttpGet( "{managerId:guid}" )]
    [ProducesResponseType( typeof( ManagerResponse ), StatusCodes.Status200OK )]
    [ProducesResponseType( StatusCodes.Status404NotFound )]
    public async Task<IActionResult> GetManagerById(
        Guid managerId,
        CancellationToken cancellationToken )
    {
        var manager = await Sender.Send(
            new GetManagerByIdQuery( managerId ),
            cancellationToken );

        return Ok( manager );
    }

    /// <summary>
    /// Получить данные о всех сотрудниках
    /// </summary>
    /// <param name="sortPageOptions">Параметры сортировки и пагинации</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns></returns>
    [HttpPost( "GetAll" )]
    [ProducesResponseType( typeof( ManagerResponse ), StatusCodes.Status200OK )]
    public async Task<IActionResult> GetManagersAll(
        SortPageOptions sortPageOptions,
        CancellationToken cancellationToken )
    {
        var managers = await Sender.Send(
            new GetAllManagersQuery( sortPageOptions ),
            cancellationToken );

        return Ok( managers );
    }

    /// <summary>
    /// Получить данные о сотрудниках согласно данным фильтра
    /// </summary>
    /// <param name="sortFilterPageOptions">Настройки фильтрации, сортировки и пагинации</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns></returns>
    [HttpPost( "Query" )]
    [ProducesResponseType( typeof( ManagerResponse ), StatusCodes.Status200OK )]
    public async Task<IActionResult> ManagersQuery(
        SortFilterPageOptions sortFilterPageOptions,
        CancellationToken cancellationToken )
    {
        var managers = await Sender.Send(
            new GetManagersByFilterQuery( sortFilterPageOptions ),
            cancellationToken );

        return Ok( managers );
    }

    /// <summary>
    /// Создать менеджера на основании запроса
    /// </summary>
    /// <param name="request">Запрос на создание менеджера</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType( typeof( ManagerResponse ), StatusCodes.Status201Created )]
    public async Task<IActionResult> CreateManager(
        CreateManagerRequest request,
        CancellationToken cancellationToken )
    {
        var command = request.Adapt<CreateManagerCommand>( );

        var manager = await Sender.Send( command, cancellationToken );

        return CreatedAtAction( nameof( GetManagerById ), new { managerId = manager.Id }, manager );
    }
}