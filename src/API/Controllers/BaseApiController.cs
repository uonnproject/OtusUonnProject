using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

/// <inheritdoc />
[ApiController]
[Route( "api/[controller]" )]
public class BaseApiController : ControllerBase
{
    /// <summary>
    /// Отправитель
    /// </summary>
    protected readonly ISender Sender;

    /// <summary>
    /// Базовый конструктор контроллеров
    /// </summary>
    /// <param name="sender">Отправитель</param>
    public BaseApiController( ISender sender )
    {
        Sender = sender;
    }
}