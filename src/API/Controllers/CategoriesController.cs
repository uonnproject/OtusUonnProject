using Application.Categories;
using Application.Core;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

///<summary> Категории </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class CategoriesController : BaseController
{
    ///<summary> Получить данные о всех категориях </summary>
    [HttpGet]
    public async Task<IActionResult> GetCategories([FromQuery] PagingParams param)
    {
        return HandlePagedResult(await Mediator.Send(new CategoryList.Query { Params = param }));
    }
}