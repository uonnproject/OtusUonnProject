using Application.Core;
using Application.Products;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

/// <summary> Продукты </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class ProductsController : BaseController
{
    ///<summary> Получить данные о всех продуктах </summary>
    [HttpGet]
    public async Task<ActionResult<ProductListDto>> GetProducts([FromQuery] PagingParams param)
    {
        return HandlePagedResult(await Mediator.Send(new ProductList.Query { Params = param }));
    }

    /// <summary> Получить данные о продукте по Id </summary>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<ProductDto>> GetProduct(Guid id)
    {
        var product = await Mediator.Send(new ProductDetails.Query { Id = id });
        if (product == null) return NotFound();
        return Ok(product);
    }

    /// <summary> Добавить продукт </summary>
    [HttpPost]
    public async Task<ActionResult> CreateProduct(ProductCreateOrEditDto product)
    {
        var result = await Mediator.Send(new ProductCreate.Command { Product = product });
        if (!result) return BadRequest("Failed to create product");
        return Ok();
    }

    /// <summary> Изменить продукт </summary>
    [HttpPut("{id:guid}")]
    public async Task<ActionResult> EditProduct(Guid id, ProductCreateOrEditDto product)
    {
        product.Id = id;
        var result = await Mediator.Send(new ProductEdit.Command { Product = product });
        if (!result) return Accepted();
        return Ok();
    }

    /// <summary> Удалить продукт </summary>
    [HttpDelete("{id:guid}")]
    public async Task<ActionResult> DeleteProduct(Guid id)
    {
        var result = await Mediator.Send(new ProductDelete.Command { Id = id });
        if (!result) return BadRequest("Failed to delete product");
        return Ok();
    }
}