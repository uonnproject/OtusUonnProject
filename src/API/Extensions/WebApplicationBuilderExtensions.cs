using Application.Managers.Commands.CreateManager;
using Application.ValidationBehaviors;
using DataAccess;
using DataAccess.Repositories;
using DataAccess.Repositories.Base;
using DataAccess.Repositories.Manager;
using DataAccess.Repositories.Product;
using Domain.Abstractions;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace API.Extensions;

/// <summary>
/// Метод расширений для WebApplicationBuilder
/// </summary>
public static class WebApplicationBuilderExtensions
{
    /// <summary>
    /// Настроить контекст работы с БД
    /// </summary>
    public static WebApplicationBuilder ConfigureContext(this WebApplicationBuilder builder)
    {
        builder.Services.AddDbContext<DataContext>(options =>
        {
            options.UseSqlServer(builder.Configuration.GetConnectionString("SqlServer"));
        });
        builder.Services.AddScoped<DbContext, DataContext>();
        return builder;
    }

    /// <summary>
    /// Добавить репозитории
    /// </summary>
    public static WebApplicationBuilder AddRepositories(this WebApplicationBuilder builder)
    {
        builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
        builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
        builder.Services.AddScoped<IManagerRepository, ManagerRepository>();
        builder.Services.AddScoped<IProductRepository, ProductRepository>();
        return builder;
    }

    /// <summary>
    /// Добавить MediatR
    /// </summary>
    public static WebApplicationBuilder AddMediatr(this WebApplicationBuilder builder)
    {
        var applicationAssembly = typeof(Application.AssemblyReference).Assembly;
        builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(applicationAssembly));
        return builder;
    }

    /// <summary>
    /// Добавить валидаторы
    /// </summary>
    public static WebApplicationBuilder AddValidators(this WebApplicationBuilder builder)
    {
        builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
        builder.Services.AddScoped<IValidator<CreateManagerCommand>, CreateManagerCommandValidator>();
        return builder;
    }
}