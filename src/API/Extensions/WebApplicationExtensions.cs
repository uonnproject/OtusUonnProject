using DataAccess;
using Microsoft.EntityFrameworkCore;
using System.Data.Entity;

namespace API.Extensions;

/// <summary>
/// Extensions for instances of WebApplication class 
/// </summary>
public static class WebApplicationExtensions
{
    /// <summary>
    /// Applies migrations to database
    /// </summary>
    /// <param name="this">Current instance of WebApplication</param>
    public static WebApplication ApplyMigrations(this WebApplication app)
    {
        using var scope = app.Services.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<DataContext>();

        //TODO temp solution for change PK to UUID
        var appliedMigrationsCount = dbContext.Database.GetAppliedMigrations().Count();
        if (appliedMigrationsCount > 1)
            dbContext.Database.EnsureDeleted();
        dbContext.Database.Migrate();
        return app;
    }

    /// <summary>
    /// Seed database with preset data
    /// </summary>
    /// <param name="this">Current instance of WebApplication</param>
    public static void SeedDatabase(this WebApplication app)
    {
        using var scope = app.Services.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<DataContext>();
        Seeder.SeedData(dbContext);
    }
}