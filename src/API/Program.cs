using API.Extensions;
using API.Middleware.Exceptions;
using Mapster;
using System.Reflection;
using System.Text.Json.Serialization;
using Application.Mappings;

var builder = WebApplication.CreateBuilder(args);

// ����� �� ����� ��� ��������, ������. ����� �� �� ���� ����������� �������, ��� ��� ����� �� �����.
if (builder == null) throw new ArgumentNullException(nameof(builder));

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(options =>
{
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory,
        $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));

    options.CustomSchemaIds(type => type.ToString());
});

builder.Services.AddMappings();

builder
    .ConfigureContext()
    .AddRepositories()
    .AddMediatr()
    .AddValidators();

builder.Services.AddCors(opt =>
{
    opt.AddPolicy("CorsPolicy", policy =>
    {
        policy
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            .WithExposedHeaders("WWW-Authenticate", "Pagination")
            .WithOrigins("http://localhost:3000");
    });
});

builder.Services.AddControllers().AddJsonOptions(x =>
{
    // serialize enums as strings in api responses (e.g. Role)
    x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());

    // ignore omitted parameters on models to enable optional params (e.g. User update)
    x.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
});

builder.Services.AddTransient<ExceptionHandlingMiddleware>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app
    .ApplyMigrations()
    .SeedDatabase();

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.UseRouting();

app.UseDefaultFiles();
app.UseStaticFiles();

app.UseCors("CorsPolicy");

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();

namespace API
{
    /// <summary>
    /// Partial class for add access from Integration tests
    /// </summary>
    public partial class Program { }
}