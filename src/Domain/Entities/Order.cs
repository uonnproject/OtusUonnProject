﻿using Domain.Entities.Base;
using Domain.Enum;

namespace Domain.Entities;

public class Order: Entity
{
    public DateTime CreationDate { get; set; }

    public Guid CustomerId { get; set; }

    public Customer Customer { get; set; }

    public Guid DeliveryId { get; set; }

    public Delivery Delivery { get; set; }

    public OrderStatus OrderStatus { get; set; }

    public string Note { get; set; }
}