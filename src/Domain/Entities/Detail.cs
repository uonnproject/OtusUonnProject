using Domain.Entities.Base;

namespace Domain.Entities;

public class Detail: Entity
{
    public Guid СategoryId { get; set; }

    public required Category Category { get; set; }

    public required Parameters Parameters { get; set; }
}