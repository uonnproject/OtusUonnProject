﻿namespace Domain.Entities;

public class Manager : User
{
    public ICollection<Product>? Products { get; set; }
}