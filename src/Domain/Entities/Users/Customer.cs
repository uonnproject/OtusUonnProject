﻿namespace Domain.Entities;

public class Customer : User
{
    public IList<Order> Orders { get; set; } = new List<Order>();
}