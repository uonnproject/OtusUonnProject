﻿using Domain.Entities.Base;

namespace Domain.Entities;

public class User : Entity
{
    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string FullName => $"{FirstName} {LastName}";

    public string? Phone { get; set; }

    public required string Email { get; set; }

    public DateTime? BirthDate { get; set; }
}