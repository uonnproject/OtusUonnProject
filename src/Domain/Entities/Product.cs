﻿using Domain.Entities.Base;

namespace Domain.Entities;

public class Product: Entity
{
    public required string Name { get; set; }

    public string Description { get; set; }

    public string Image { get; set; }

    public Guid CategoryId { get; set; }

    public Category Category { get; set; }

    public double Price { get; set; }
        
    public int Quantity { get; set; }
      
    public Guid ManagerId { get; set; }
    
    public Manager Manager { get; set; }
}