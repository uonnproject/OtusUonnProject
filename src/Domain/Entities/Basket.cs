﻿namespace Domain.Entities;

public class Basket
{
    public Guid CustomerId { get; set; }

    public Customer Customer { get; set; } = null!;
        
    public Guid ProductId { get; set; }
        
    public Product Product { get; set; } = null!;
        
    public int Quantity { get; set; }
}