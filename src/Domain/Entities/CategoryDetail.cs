﻿using Domain.Entities.Base;

namespace Domain.Entities;

public class CategoryDetail: Entity
{
    public Guid CategoryId { get; set; }

    public required Category Category { get; set; }

    public string Parameters { get; set; }
}