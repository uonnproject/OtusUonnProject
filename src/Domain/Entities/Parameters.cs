﻿using System.Text.Json.Serialization;

namespace Domain.Entities;

public class Parameters
{
    public string Name { get; set; }

    public string Color { get; set; }

    public string Description { get; set; }
}