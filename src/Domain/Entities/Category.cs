﻿using Domain.Entities.Base;

namespace Domain.Entities;

public class Category : Entity
{
    public Guid? ParentId { get; set; } = null;

    public Category Parent { get; set; }

    public required string Name { get; set; }

    public ICollection<Product> Products { get; set; }
}