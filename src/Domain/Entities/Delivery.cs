﻿using Domain.Entities.Base;
using Domain.Enum;

namespace Domain.Entities;

public class Delivery: Entity
{
    public DateTime DateDelivery { get; set; }

    public DeliveryStatus DeliveryStatus {  get; set; }
        
    public string Address {  get; set; }
}