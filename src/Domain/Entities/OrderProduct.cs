﻿namespace Domain.Entities;

public class OrderProduct
{
    public Guid ProductId { get; set; }
    public Product Product { get; set; }
    public Guid OrderId { get; set; }
    public double Quantity { get; set; }
}