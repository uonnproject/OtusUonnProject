namespace Domain.Exceptions;

public class ManagerNotFoundException : NotFoundException
{
    public ManagerNotFoundException( Guid id )
        : base( $"Менеджер с идентификатором {id} не найден." )
    {
    }
}