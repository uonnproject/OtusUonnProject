namespace Domain.Exceptions;

public abstract class NotFoundException : ApplicationException
{
    public NotFoundException(
        string message) 
        : base("Not Found", message) //TODO: move title to constants or resources
    {
    }
}