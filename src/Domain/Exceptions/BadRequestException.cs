namespace Domain.Exceptions;

public abstract class BadRequestException : ApplicationException
{
    public BadRequestException(string message) 
        : base("Bad Request", message) //TODO: move title to constants or resources
    {
    }
}