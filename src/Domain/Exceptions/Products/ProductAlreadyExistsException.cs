namespace Domain.Exceptions.Products;

public class ProductAlreadyExistsException : BadRequestException
{
    public ProductAlreadyExistsException( Guid id )
        : base( "Продукт с таким идентификатором {id} уже существует." )
    {
    }
}