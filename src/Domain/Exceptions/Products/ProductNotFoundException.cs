namespace Domain.Exceptions.Products;

public class ProductNotFoundException : NotFoundException
{
    public ProductNotFoundException( Guid id )
        : base( $"Продукт с идентификатором {id} не найден." )
    {
    }
}