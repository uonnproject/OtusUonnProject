﻿namespace Domain.Enum;

public enum OrderStatus
{
    Processed,
    AwaitingPayment,
    Collected,
    AwaitingShipment,
    OnTheWay,
    Issued
}