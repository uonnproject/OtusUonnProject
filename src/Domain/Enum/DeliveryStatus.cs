﻿namespace Domain.Enum;

public enum DeliveryStatus
{
    AwaitingShipment,
    OnTheWay,
    Issued
}