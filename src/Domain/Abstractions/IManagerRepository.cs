using DataAccess.Abstractions.Managers;
using Domain.Entities;

namespace Domain.Abstractions;

public interface IManagerRepository
    : IRepository<Manager>
{
    Task<List<Manager>> QueryByFilterAsync(
        SortFilterPageOptions sortFilterPageOptions,
        CancellationToken cancellationToken );

    Task<List<Manager>> GetAllAsync(
        SortPageOptions sortPageOptions,
        CancellationToken cancellationToken );
}