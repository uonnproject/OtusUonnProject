using Domain.Entities;

namespace Domain.Abstractions;

public interface IOrderRepository
{
    Task CreateAsync(Order order, CancellationToken cancellationToken);
}