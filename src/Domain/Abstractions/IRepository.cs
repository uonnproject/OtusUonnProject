using Domain.Entities.Base;

namespace Domain.Abstractions;

public interface IRepository<TEntity>
    where TEntity : Entity
{
    Task<TEntity> AddAsync(
        TEntity entity,
        CancellationToken cancellationToken );

    Task AddRangeAsync(
        IEnumerable<TEntity> entities,
        CancellationToken cancellationToken );

    Task<TEntity?> GetByIdAsync(
        Guid id,
        CancellationToken cancellationToken );

    Task<List<TEntity>> GetByIdsAsync(
        IEnumerable<Guid> ids,
        CancellationToken cancellationToken,
        bool asNoTracking = false );

    void UpdateAsync(
        TEntity entity,
        CancellationToken cancellationToken );

    void UpdateRangeAsync(
        IEnumerable<TEntity> entities,
        CancellationToken cancellationToken );

    void Delete( TEntity entity );

    void DeleteRange( IEnumerable<TEntity> entities );
}