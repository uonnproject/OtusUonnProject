using DataAccess.Abstractions.Products;
using Domain.Entities;

namespace Domain.Abstractions;

public interface IProductRepository
    : IRepository<Product>
{
    Task<List<Product>> GetAllAsync(
        SortPageOptions sortPageOptions,
        CancellationToken cancellationToken );
    
    Task ReduceQuantity(
        Guid productId, 
        CancellationToken cancellationToken);
}