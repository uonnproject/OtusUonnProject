﻿using Application.Products;
using Domain.Entities;
using Mapster;

namespace Application.Mappings;

public sealed class GlobalMappingConfig : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<Product, ProductDto>()
            .Map(pdto => pdto.Category, p => p.Category.Name)
            .Map(pdto => pdto.Manager, p => p.Manager.FullName)
            .RequireDestinationMemberSource(true);

        config.NewConfig<Product, ProductListDto>()
            .Map(pdto => pdto.Category, p => p.Category.Name)
            .Map(pdto => pdto.Category, p => p.Category.Name)
            .RequireDestinationMemberSource(true);

        config.NewConfig<ProductCreateOrEditDto, Product>()
            .IgnoreNullValues(true)
            .Map(p => p.Category, pdto => new Category { Name = pdto.Category });

    }
}
