using Application.Core;
using DataAccess;
using Mapster;
using MediatR;

namespace Application.Products;

public class ProductList
{
    public class Query : IRequest<PagedList<ProductListDto>>
    {
        public PagingParams Params { get; set; }
    }

    public class Handler : IRequestHandler<Query, PagedList<ProductListDto>>
    {
        private readonly DataContext _context;
        public Handler(DataContext context)
        {
            _context = context;
        }

        public async Task<PagedList<ProductListDto>> Handle(Query request, CancellationToken cancellationToken)
        {
            var query = _context.Products.ProjectToType<ProductListDto>();
            return await PagedList<ProductListDto>.CreateAsync(query, request.Params.PageNumber, request.Params.PageSize);
        }
    }
}