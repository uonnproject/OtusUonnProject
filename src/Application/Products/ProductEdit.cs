using DataAccess;
using Domain.Entities;
using MediatR;

namespace Application.Products;

public class ProductEdit
{
    public class Command : IRequest<bool>
    {
        public ProductCreateOrEditDto Product { get; set; }
    }

    public class Handler : IRequestHandler<Command, bool>
    {
        private readonly DataContext _context;

        public Handler(DataContext context)
        {
            _context = context;
        }

        async Task<bool> IRequestHandler<Command, bool>.Handle(Command request, CancellationToken cancellationToken)
        {
            var category = request.Product.Category.Trim();
            var product = await _context.Products.FindAsync(request.Product.Id);
            if (product == null) return false;

            product.Name = request.Product.Name;
            product.Description = request.Product.Description;
            product.Image = request.Product.Image;
            product.Price = request.Product.Price;
            product.Quantity = request.Product.Quantity;

            var alreadyExistsCategory = _context.Categories.Where(x => x.Name == category).FirstOrDefault();
            if (alreadyExistsCategory == null)
            {
                product.Category = new Category { Name = category };
            }
            else
            {
                product.Category = alreadyExistsCategory;
            }

            var result = await _context.SaveChangesAsync() > 0;
            return result;
        }
    }
}
