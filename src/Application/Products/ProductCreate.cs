using DataAccess;
using Domain.Entities;
using Mapster;
using MediatR;

namespace Application.Products;

public class ProductCreate
{
    public class Command : IRequest<bool>
    {
        public ProductCreateOrEditDto Product { get; set; }
    }

    public class Handler : IRequestHandler<Command, bool>
    {
        private readonly DataContext _context;
        public Handler(DataContext context)
        {
            _context = context;
        }

        async Task<bool> IRequestHandler<Command, bool>.Handle(Command request, CancellationToken cancellationToken)
        {
            var category = request.Product.Category.Trim();
            var product = request.Product.Adapt<Product>();
            product.Manager = _context.Managers.First();

            var alreadyExistsCategory = _context.Categories.Where(x => x.Name == category).FirstOrDefault();
            if (alreadyExistsCategory != null)
            {
                product.Category = alreadyExistsCategory;
            }
            _context.Products.Add(product);
            return await _context.SaveChangesAsync() > 0;
        }
    }
}