using DataAccess;
using MediatR;

namespace Application.Products;

public class ProductDelete
{
    public class Command : IRequest<bool>
    {
        public Guid Id { get; set; }
    }

    public class Handler : IRequestHandler<Command, bool>
    {
        private readonly DataContext _context;
        public Handler(DataContext context)
        {
            _context = context;
        }

        async Task<bool> IRequestHandler<Command, bool>.Handle(Command request, CancellationToken cancellationToken)
        {
            var product = await _context.Products.FindAsync(request.Id);
            if (product == null) return false;
            _context.Remove(product);
            return await _context.SaveChangesAsync() > 0;
        }
    }
}