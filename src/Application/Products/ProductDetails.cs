using DataAccess;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Products;

public class ProductDetails
{
    public class Query : IRequest<ProductDto>
    {
        public Guid Id { get; set; }
    }

    public class Handler : IRequestHandler<Query, ProductDto>
    {
        private readonly DataContext _context;

        public Handler(DataContext context)
        {
            _context = context;
        }

        public async Task<ProductDto> Handle(Query request, CancellationToken cancellationToken)
        {
            var product = await _context.Products.Where(x => x.Id == request.Id)
                .Include(x => x.Category)
                .Include(x => x.Manager)
                .FirstOrDefaultAsync();
            return product.Adapt<ProductDto>();
        }
    }
}