using Application.Abstractions;

namespace Application.Helpers;

public static class PagedResponseHelper
{
    public static PagedResponse<TResponseItem> CreateEmptyResponse<TResponseItem>( )
    {
        return new PagedResponse<TResponseItem>(
            Array.Empty<TResponseItem>( ),
            pageNumber: 1,
            allPages: 0 );
    }
}