namespace Application.Managers.Response;

public class ManagerResponse
{
    public Guid Id { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string FullName => $"{FirstName} {LastName}";

    public string? Phone { get; set; }

    public required string Email { get; set; }

    public DateTime? BirthDate { get; set; }
}