using Application.Contracts;

namespace Application.Managers.Contracts;

public class ManagerDigestDto : BaseDto
{
    public required string FullName { get; set; }

    public string? Phone { get; set; }

    public required string Email { get; set; }
}