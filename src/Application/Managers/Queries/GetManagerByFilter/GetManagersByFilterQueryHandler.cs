using Application.Abstractions;
using Application.Managers.Response;
using Domain.Abstractions;
using Mapster;

namespace Application.Managers.Queries.GetManagerByFilter;

public class GetManagersByFilterQueryHandler
    : IQueryHandler<GetManagersByFilterQuery, PagedResponse<ManagerResponse>>
{
    private readonly IManagerRepository _managerRepository;

    public GetManagersByFilterQueryHandler( IManagerRepository managerRepository ) =>
        _managerRepository = managerRepository;

    public async Task<PagedResponse<ManagerResponse>> Handle(
        GetManagersByFilterQuery request,
        CancellationToken cancellationToken )
    {
        var managers = await _managerRepository.QueryByFilterAsync(
            request.SortFilterPageOptions,
            cancellationToken );

        if ( !managers.Any( ) )
            return new PagedResponse<ManagerResponse>( Array.Empty<ManagerResponse>( ), 0, 0 );

        var managerResponse = managers.Adapt<List<ManagerResponse>>( );

        return new PagedResponse<ManagerResponse>(
            managerResponse,
            request.SortFilterPageOptions.Pagination.PageNumber,
            request.SortFilterPageOptions.Pagination.NumberPages );
    }
}