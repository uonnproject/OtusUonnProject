using Application.Abstractions;
using Application.Managers.Response;
using DataAccess.Abstractions.Managers;

namespace Application.Managers.Queries.GetManagerByFilter;

public sealed record GetManagersByFilterQuery( SortFilterPageOptions SortFilterPageOptions )
    : IQuery<PagedResponse<ManagerResponse>>;