using Application.Abstractions;
using Application.Managers.Response;
using Domain.Abstractions;
using Domain.Exceptions;
using Mapster;

namespace Application.Managers.Queries.GetManagerById;

internal sealed class GetManagerByIdQueryHandler
    : IQueryHandler<GetManagerByIdQuery, ManagerResponse>
{
    private readonly IManagerRepository _managerRepository;

    public GetManagerByIdQueryHandler( IManagerRepository managerRepository ) =>
        _managerRepository = managerRepository;

    public async Task<ManagerResponse> Handle(
        GetManagerByIdQuery request,
        CancellationToken cancellationToken )
    {
        var manager = await _managerRepository.GetByIdAsync( request.ManagerId, cancellationToken );

        if ( manager == null )
        {
            throw new ManagerNotFoundException( request.ManagerId );
        }

        return manager.Adapt<ManagerResponse>( );
    }
}