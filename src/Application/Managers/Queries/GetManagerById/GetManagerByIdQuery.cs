using Application.Abstractions;
using Application.Managers.Response;

namespace Application.Managers.Queries.GetManagerById;

public sealed record GetManagerByIdQuery( Guid ManagerId )
    : IQuery<ManagerResponse>;