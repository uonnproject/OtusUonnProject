using Application.Abstractions;
using Application.Helpers;
using Application.Managers.Response;
using Domain.Abstractions;
using Mapster;

namespace Application.Managers.Queries.GetAllManagers;

public class GetAllManagersQueryHandler
    : IQueryHandler<GetAllManagersQuery, PagedResponse<ManagerResponse>>
{
    private readonly IManagerRepository _managerRepository;

    public GetAllManagersQueryHandler( IManagerRepository managerRepository ) =>
        _managerRepository = managerRepository;

    public async Task<PagedResponse<ManagerResponse>> Handle(
        GetAllManagersQuery request,
        CancellationToken cancellationToken )
    {
        var managers = await _managerRepository.GetAllAsync(
            request.SortPageOptions,
            cancellationToken );

        if ( !managers.Any( ) )
            return PagedResponseHelper.CreateEmptyResponse<ManagerResponse>( );

        var managerResponse = managers.Adapt<List<ManagerResponse>>( );

        return new PagedResponse<ManagerResponse>(
            managerResponse,
            request.SortPageOptions.Pagination.PageNumber,
            request.SortPageOptions.Pagination.NumberPages );
    }
}