using Application.Abstractions;
using Application.Managers.Response;
using DataAccess.Abstractions.Managers;

namespace Application.Managers.Queries.GetAllManagers;

public sealed record GetAllManagersQuery( SortPageOptions SortPageOptions )
    : IQuery<PagedResponse<ManagerResponse>>;