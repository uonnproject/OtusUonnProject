using Application.Abstractions;
using Application.Managers.Response;

namespace Application.Managers.Commands.CreateManager;

public sealed record CreateManagerCommand
    : ICommand<ManagerResponse>
{
    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? Phone { get; set; }

    public required string Email { get; set; }

    public DateTime? BirthDate { get; set; }
}