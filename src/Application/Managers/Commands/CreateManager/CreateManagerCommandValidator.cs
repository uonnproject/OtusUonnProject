using FluentValidation;

namespace Application.Managers.Commands.CreateManager;

public sealed class CreateManagerCommandValidator
    : AbstractValidator<CreateManagerCommand>
{
    public CreateManagerCommandValidator( )
    {
        RuleFor( x => x.Email )
            .EmailAddress( )
            .NotNull( )
            .NotEmpty( )
            .MaximumLength( 250 );

        RuleFor( x => x.FirstName )
            .NotEmpty( )
            .Matches( ValidationConstants.RegExp.OnlyLetters )
            .WithMessage( "Поле '{PropertyName}' должно содержать только буквы" )
            .Length( 2, 100 );
        
        RuleFor( x => x.LastName )
            .NotEmpty( )
            .Matches( ValidationConstants.RegExp.OnlyLetters  )
            .WithMessage( "Поле '{PropertyName}' должно содержать только буквы" )
            .Length( 2, 100 );
    }
}