using System.Text.Json.Serialization;

namespace Application.Managers.Commands.CreateManager;

public sealed record CreateManagerRequest
{
    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? Phone { get; set; }

    [JsonRequired]
    public required string Email { get; set; }

    public DateTime? BirthDate { get; set; }
}