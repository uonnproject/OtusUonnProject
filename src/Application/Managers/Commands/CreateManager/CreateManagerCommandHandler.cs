using Application.Abstractions;
using Application.Managers.Response;
using Domain.Abstractions;
using Domain.Entities;
using Mapster;

namespace Application.Managers.Commands.CreateManager;

internal sealed class CreateManagerCommandHandler
    : ICommandHandler<CreateManagerCommand, ManagerResponse>
{
    private readonly IManagerRepository _managerRepository;
    private readonly IUnitOfWork _unitOfWork;

    public CreateManagerCommandHandler(
        IManagerRepository managerRepository,
        IUnitOfWork unitOfWork )
    {
        _managerRepository = managerRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<ManagerResponse> Handle(
        CreateManagerCommand request,
        CancellationToken cancellationToken )
    {
        var managerAdded = await _managerRepository.AddAsync(
            request.Adapt<Manager>( ),
            cancellationToken );

        await _unitOfWork.SaveChangesAsync( cancellationToken );

        return managerAdded.Adapt<ManagerResponse>( );
    }
}