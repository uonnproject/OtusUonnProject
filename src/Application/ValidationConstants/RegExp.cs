using System.Text.RegularExpressions;

namespace Application.ValidationConstants;

public static partial class RegExp
{
    public static readonly Regex OnlyLetters = _onlyLettersRegex();

    [GeneratedRegex("^[a-zA-ZА-Яа-я]+$", RegexOptions.Compiled)]
    private static partial Regex _onlyLettersRegex();
}