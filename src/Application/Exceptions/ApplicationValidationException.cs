namespace Application.Exceptions;
using ApplicationException = Domain.Exceptions.ApplicationException;

public sealed class ApplicationValidationException : ApplicationException
{
    public ApplicationValidationException(IReadOnlyDictionary<string, string[]>? errorsDictionary)
        : base("Validation Failure", "One or more validation errors occurred") //TODO: move to constants or resources
        => ErrorsDictionary = errorsDictionary;

    public IReadOnlyDictionary<string, string[]>? ErrorsDictionary { get; }
}