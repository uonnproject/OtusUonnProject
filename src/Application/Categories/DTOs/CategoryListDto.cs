using Domain.Entities;

namespace Application.Categories;

public class CategoryListDto
{
    public Guid? ParentId { get; set; } = null;

    public Category Parent { get; set; }

    public required string Name { get; set; }

    public ICollection<Product> Products { get; set; }
}
