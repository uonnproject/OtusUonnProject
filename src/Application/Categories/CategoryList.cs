using Application.Core;
using Application.Products;
using DataAccess;
using Mapster;
using MediatR;

namespace Application.Categories;

public class CategoryList
{
    public class Query : IRequest<PagedList<CategoryListDto>>
    {
        public PagingParams Params { get; set; }
    }

    public class Handler : IRequestHandler<Query, PagedList<CategoryListDto>>
    {
        private readonly DataContext _context;
        public Handler(DataContext context)
        {
            _context = context;
        }

        public async Task<PagedList<CategoryListDto>> Handle(Query request, CancellationToken cancellationToken)
        {
            var query = _context.Categories.ProjectToType<CategoryListDto>();
            return await PagedList<CategoryListDto>.CreateAsync(query, request.Params.PageNumber, request.Params.PageSize);
        }
    }
}
