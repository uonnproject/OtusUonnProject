namespace Application.Abstractions;

public class PagedResponse<TResponseItem>
{
    public IEnumerable<TResponseItem> Items { get; set; }

    public int PageNumber { get; set; }

    public int AllPages { get; set; }

    public PagedResponse(
        IEnumerable<TResponseItem> items,
        int pageNumber,
        int allPages)
    {
        Items = items;
        PageNumber = pageNumber;
        AllPages = allPages;
    }

    private void EmptyPagedResponse( )
    {
        Items = Array.Empty<TResponseItem>();
        PageNumber = 1;
        AllPages = 0;
    }
}