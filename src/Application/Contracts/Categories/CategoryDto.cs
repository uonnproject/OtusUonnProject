namespace Application.Contracts.Categories;

public class CategoryDto : BaseDto
{
    public Guid? ParentId { get; set; }
    
    public CategoryDto? Parent { get; set; }

    public required string Name { get; set; }

    public int Type { get; set; }

    public List<CategoryDto>? Children { get; set; }
}