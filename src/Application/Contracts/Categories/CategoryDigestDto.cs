namespace Application.Contracts.Categories;

public class CategoryDigestDto : BaseDto
{
    public required string Name { get; set; }

    public int Type { get; set; }
}