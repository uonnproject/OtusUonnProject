﻿using Microsoft.EntityFrameworkCore;
using DataAccess.EntityConfigurations;
using Domain.Entities;

namespace DataAccess;

public class DataContext : DbContext
{
    public DbSet<Manager> Managers { get; set; }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Order>  Order { get; set; }
    public DbSet<Delivery> Deliveries { get; set; }
    public DbSet<Detail> Details { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<OrderProduct> OrdersProduct { get; set; }
    public DbSet<CategoryDetail> CategoriesDetail { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Basket> Baskets { get; set; }

    public DataContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.SettingUpRelationships();

        // modelBuilder.ApplyConfiguration(new UserEntityConfiguration());
        modelBuilder.ApplyConfiguration(new CustomerEntityConfiguration());
        modelBuilder.ApplyConfiguration(new ManagerEntityConfiguration());
        modelBuilder.ApplyConfiguration(new ProductEntityConfiguration());
        modelBuilder.ApplyConfiguration(new BasketEntityConfiguration());
        modelBuilder.ApplyConfiguration(new CategoryDetailEntityConfiguration());
        modelBuilder.ApplyConfiguration(new CategoryEntityConfiguration());
        modelBuilder.ApplyConfiguration(new DeliveryEntityConfiguration());
        modelBuilder.ApplyConfiguration(new OrderEntityConfiguration());
        modelBuilder.ApplyConfiguration(new OrderProductEntityConfiguration());

        base.OnModelCreating(modelBuilder);
    }
}
