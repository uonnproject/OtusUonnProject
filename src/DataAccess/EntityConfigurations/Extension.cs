﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.EntityConfigurations;

public static class Extension
{
    public static void SettingUpRelationships(this ModelBuilder modelBuilder)
    {
        //modelBuilder
        //.Entity<User>()
        //.HasOne(u => u.Manager)
        //.WithOne(p => p.User)
        //.HasForeignKey<Manager>(p => p.UserId);


        //modelBuilder
        //    .Entity<User>()
        //    .HasOne(u => u.Customer)
        //    .WithOne(p => p.User)
        //    .HasForeignKey<Customer>(p => p.UserId);

        modelBuilder
            .Entity<Order>()
            .HasOne(u => u.Customer)
            .WithMany(p => p.Orders);
        //.<Customer>(p => p.Id);

        modelBuilder
            .Entity<Detail>()
            .OwnsOne(post => post.Parameters, builder => { builder.ToJson(); });

    }
}