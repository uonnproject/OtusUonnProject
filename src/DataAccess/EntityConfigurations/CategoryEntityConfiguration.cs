﻿using DataAccess.EntityConfigurations.Base;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.EntityConfigurations;

internal class CategoryEntityConfiguration : BaseEntityTypeConfiguration<Category>
{
    public override void Configure( EntityTypeBuilder<Category> builder )
    {
        base.Configure( builder );

        builder.ToTable( "Categories" );

        builder
            .HasMany( c => c.Products )
            .WithOne( p => p.Category )
            .HasForeignKey( p => p.CategoryId )
            .IsRequired( );
    }
}