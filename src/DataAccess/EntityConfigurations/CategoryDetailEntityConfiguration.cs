﻿using DataAccess.EntityConfigurations.Base;
using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.EntityConfigurations;

internal class CategoryDetailEntityConfiguration : BaseEntityTypeConfiguration<CategoryDetail>
{
    public override void Configure(EntityTypeBuilder<CategoryDetail> builder)
    {
        base.Configure(builder);
            
        builder.ToTable("CategoriesDetail");
            
        builder.HasIndex(r => r.CategoryId);
    }
}