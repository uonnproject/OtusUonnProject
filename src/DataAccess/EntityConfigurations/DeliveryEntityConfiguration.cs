﻿using DataAccess.EntityConfigurations.Base;
using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.EntityConfigurations;

internal class DeliveryEntityConfiguration : BaseEntityTypeConfiguration<Delivery>
{
    public override void Configure(EntityTypeBuilder<Delivery> builder)
    {
        builder.ToTable("Deliveries");
            
        builder.Property(r => r.Address).IsRequired().HasMaxLength(200);
    }
}