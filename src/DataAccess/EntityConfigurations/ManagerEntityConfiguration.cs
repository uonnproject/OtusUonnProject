﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.EntityConfigurations;

public class ManagerEntityConfiguration : IEntityTypeConfiguration<Manager>
{
    public void Configure(EntityTypeBuilder<Manager> builder)
    {
        builder.ToTable("Managers");
        
        builder.Property( x => x.Email ).HasMaxLength( 250 );
        
        builder
            .HasMany(e => e.Products)
            .WithOne(e => e.Manager)
            .HasForeignKey(e => e.ManagerId)
            .IsRequired();
        
    }
}