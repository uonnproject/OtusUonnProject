using DataAccess.EntityConfigurations.Base;
using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.EntityConfigurations;

internal class DetailEntityConfiguration : BaseEntityTypeConfiguration<Detail>
{
    public override void Configure(EntityTypeBuilder<Detail> builder)
    {
        base.Configure(builder);
        builder.ToTable("Detail");
            
        builder.HasIndex(r => r.СategoryId);

        builder
            .OwnsOne (post => post.Parameters, builder => { builder.ToJson(); });
        //NVARCHAR(4000)
        //builder.Property(u => u.Parametrs).HasColumnType("NVARCHAR(4000)");
    }
}