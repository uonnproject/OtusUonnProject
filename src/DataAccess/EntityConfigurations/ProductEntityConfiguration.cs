﻿using DataAccess.EntityConfigurations.Base;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.EntityConfigurations;

internal class ProductEntityConfiguration : BaseEntityTypeConfiguration<Product>
{
    public override void Configure(EntityTypeBuilder<Product> builder)
    {
        base.Configure(builder);
            
        builder.ToTable("Products");       

        builder.Property(r => r.Name).IsRequired().HasMaxLength(500);

        builder.Property(u => u.Price).HasColumnType("money");
    }
}