using Domain.Entities;
using LinqKit;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.FilterBuilders;

public static class ManagerFilterPredicateBuilder<TEntity>
    where TEntity : Manager
{
    public static ExpressionStarter<TEntity> Build( Abstractions.Managers.ManagerFilter? managerFilter )
    {
        var predicate = PredicateBuilder.New<TEntity>( true );

        if ( managerFilter == null )
            return predicate;

        if ( !string.IsNullOrWhiteSpace( managerFilter.Email ) )
            predicate = predicate.And( x => EF.Functions.Like( x.Email, $"%{managerFilter.Email.Trim( )}%" ) );

        if ( !string.IsNullOrWhiteSpace( managerFilter.FirstName ) )
            predicate = predicate.And( x => x.FirstName != null
                                            && EF.Functions.Like(
                                                x.FirstName,
                                                $"{managerFilter.FirstName.Trim( )}%" ) );

        if ( !string.IsNullOrWhiteSpace( managerFilter.LastName ) )
            predicate = predicate.And( x => x.LastName != null
                                            && EF.Functions.Like(
                                                x.LastName,
                                                $"{managerFilter.LastName.Trim( )}%" ) );

        if ( !string.IsNullOrWhiteSpace( managerFilter.Phone ) )
            predicate = predicate.And( x => x.Phone != null
                                            && x.Phone == managerFilter.Phone );

        if ( managerFilter.BirthDateFrom != null )
            predicate = predicate.And( x => x.BirthDate >= managerFilter.BirthDateFrom );

        if ( managerFilter.BirthDateTo != null )
            predicate = predicate.And( x => x.BirthDate <= managerFilter.BirthDateTo );
        
        return predicate;
    }
}