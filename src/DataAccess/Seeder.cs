using Domain.Entities;

namespace DataAccess;

public class Seeder
{
    public static void SeedData(DataContext context)
    {
        if (!context.Products.Any())
        {
            var admin = new Manager
            {
                Id = Guid.NewGuid(),
                FirstName = "Иван",
                LastName = "Иванов",
                Email = "ivanivanov@gmail.com",
            };
            var manager = new Manager
            {
                Id = Guid.NewGuid(),
                FirstName = "Максим",
                LastName = "Петров",
                Email = "PetrPetrov@gmail.com",
            };

            var smartPhoneCategory = new Category { Id = Guid.NewGuid(), Name = "Смартфоны" };
            var notebookCategory = new Category { Id = Guid.NewGuid(), Name = "Игровые ноутбуки" };
            var mouseCategory = new Category { Id = Guid.NewGuid(), Name = "Мыши игровы" };
            var keyboardCategory = new Category { Id = Guid.NewGuid(), Name = "Клавиатуры" };

            var products = new List<Product>()
            {
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "15.6 Игровой ноутбук ASUS TUF Gaming F15 FX506HF-HN017, Intel Core i5-11400H (2.7 ГГц), RAM 16 ГБ, SSD 512 ГБ, NVIDIA GeForce RTX 2050",
                    Description = "<div class=\"app-catalog-8zawkn e1o7ytu50\"><p>Ноутбук игровой ASUS TUF Gaming F15 FX506HF-HN027 укомплектован шестиядерным процессором Core i5 11-го поколения, который работает в паре с дискретной видеокартой GeForce RTX 2050 и 16 Гб оперативной памяти. Вычислительной мощности аппаратной «начинки» достаточно, чтобы запускать компьютерные игры 2021-22 гг. на максимальных настройках графики.</p> <p>Поддержка сверхбыстрого стандарта Wi-Fi 6 позволяет участвовать в соревновательных онлайн-играх без задержек и зависания. При необходимости компьютер можно подключить к внешнему монитору через видеовыход HDMI или интерфейс Thunderbolt 4.</p> <p><strong>Особенности</strong></p> <ul> <li>ударопрочный корпус;</li> <li>эргономичная клавиатура с настраиваемой RGB-подсветкой;</li> <li>активное охлаждение;</li> <li>внутренние динамики.</li> </ul> <p>Твердотельный накопитель объемом 512 гигабайт гарантирует мгновенную загрузку операционной системы и плавную работу установленных приложений, а также обеспечивает быстрый доступ к сохраненным файлам, поэтому ничего не будет тормозить даже при длительной нагрузке. Преимуществом данной модели является возможность апгрейда: можно увеличить количество ОЗУ до 32 Гбайт и установить второй SSD-диск M.2.</p> <p><strong>Качественный FHD-дисплей</strong></p> <p>15,6-дюймовая матрица отличается малым временем отклика, а также поддерживает разрешение 1920х1080 пикселей и частоту обновления 144 Гц, благодаря чему изображение получается четким, а динамичные сцены воспроизводятся без разрыва кадров. Широкие углы обзора и соотношение сторон 16:9 оптимально подходят для работы в браузере и просмотра фильмов. Благодаря поддержке популярных цветовых охватов sRGB, NTSC и Adobe RGB можно заниматься редактированием фотографий.</p> <p><strong>Автономность</strong></p> <p>В ноутбуке установлена литий-ионная батарея энергоемкостью 48 Wh, которая способна продержаться до 5 часов без подзарядки. Однако при запуске ресурсоемких игр аккумулятора хватит только на 1-2 ч.</p></div>",
                    Price = 78759,
                    Quantity = 22,
                    Category = notebookCategory,
                    Manager = manager,
                    Image = "https://ir.ozone.ru/s3/multimedia-q/wc1000/6306434582.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Игровой ноутбук MSI Titan GT77 12UHS-200RU",
                    Description = "<div>Игровой ноутбук <b>MSI Titan GT77 12UHS-200RU</b> оснащен дисплеем с матрицей IPS-level и разрешением 3840x2160 пикселей. За счет этого модель выводит яркое детализированное изображение. Матовое покрытие предотвращает образование бликов при ярком солнечном свете, что обеспечивает оптимальную видимость изображения на экране при любом уровне освещения. Это обеспечивает комфортные условия эксплуатации. Устройство характеризуется высокой производительностью и энергоэффективностью благодаря 16-ядерному процессору Core i9-12900HX.<br><br>Игровой ноутбук MSI Titan GT77 12UHS-200RU имеет 64 ГБ оперативной памяти с возможностью расширения объема до 128 ГБ. Благодаря этому можно одновременно запускать несколько окон браузера и энергоемких программ без зависания картинки. Для хранения контента предусмотрен SSD-накопитель объемом 2 ТБ. Встроенные динамики и звуковая система типа Dynaudio обеспечивают чистый звук без помех и искажений.<br><br>Игровой ноутбук MSI Titan GT77 12UHS-200RU имеет сканер отпечатков пальцев, который отвечает за удобную разблокировку экрана и безопасность личных данных. Модуль Wi-Fi обеспечивает стабильное подключение к интернету для поиска необходимой информации, скроллинга различного контента и общения в социальных сетях. Посредством веб-камеры можно принимать участие в видеоконференциях. Устройство оснащено аккумулятором емкостью 6250 мА*ч, чего достаточно для автономной работы модели в течение 5 ч.<br><br><div></div><br></div>",
                    Price = 309999,
                    Quantity = 22,
                    Category = notebookCategory,
                    Manager = manager,
                    Image = "https://static.eldorado.ru/img1/b/bb/77466300.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Игровая мышь беспроводная Logitech G G502 LIGHTSPEED",
                    Description = "Мышь беспроводная/проводная Logitech G502 LIGHTSPEED черная [910-005567] привлекает внимание футуристичным дизайном, удобным хватом правой рукой и моментальным откликом на любые команды. Благодаря датчику HERO 25K производительность улучшена до 25600 dpi. Также ключом к победе станут фильтрация, ускорение и нулевое сглаживание, дающие полный контроль над игрой.\r\nМодель Logitech G502 LIGHTSPEED оснащена быстрым скролом и 11-ю кнопками с удобной настройкой игрового процесса. Мышь идет в комплекте со съемными утяжелителями для настройки веса. RGB-подсветка с поддержкой более 16.8 млн цветов легко подстраивается под ваши пожелания. Полной емкости АКБ хватает на 60 ч работы, а всего 5 минут зарядки достаточно для 2.5 часа игры. Еще одно отличие мыши [910-005567] – наличие сертификата Plastic Neutral, подтверждающего отсутствие ПВХ и ваш вклад в программу очистки Мирового океана от пластика Plastic Bank.",
                    Price = 9862,
                    Quantity = 31,
                    Category = mouseCategory,
                    Manager = manager,
                    Image = "https://ir.ozone.ru/s3/multimedia-l/wc1000/6425089317.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Смартфон HUAWEI Mate X3 12/512GB, Черный",
                    Description = "<div class=\"product-description-basic__content\"><p>\t Технологичный смартфон Huawei Mate X3 — превосходный инструмент для работы, учебы или ведения бизнеса. Благодаря инновационной складной конструкции экран может раскладываться, увеличиваясь до размеров планшета. От царапин и трещин его защищает ударопрочное стекло Kunlun. Устройство поддерживает установку двух SIM-карт типа nano.</p><div style=\"text-align: center;\"> <img src=\"https://shop.mts.ru/upload/images/smartfon-huawei-mate-x3-1.jpg\"></div><h3>Инновационные дисплеи</h3><p>\t Huawei Mate X3 получил два интеллектуальных светочувствительных OLED-экрана с цветовой палитрой 1.07 миллиардов цветов. Внешний экран с диагональю 6.4 дюйма воспроизводит яркую и четкую картинку в разрешении 2504х1080 пикселей. В развернутом виде становится доступен 7.85-дюймовый с разрешением 2496х2224 точек. Частота обновления экранов 120 Гц позволяет наслаждаться плавным скроллингом и динамичными сценами без рывков.</p><div style=\"text-align: center;\"> <img src=\"https://shop.mts.ru/upload/images/smartfon-huawei-mate-x3-2.jpg\"></div><h3>Мощная система камер</h3><p>\t Смартфон оснащен основной камерой 50 Мп, дополненной 13-мегапиксельной сверхширокоугольной камерой и 12-мегапиксельным телеобъективом с оптической стабилизацией изображения. Встроен цифровой, оптический и гибридный зум. На основную камеру смартфон поддерживает съемку видео в формате 4К.</p><div style=\"text-align: center;\"> <img src=\"https://shop.mts.ru/upload/images/smartfon-huawei-mate-x3-3.jpg\"></div><p>\t На двух экранах в круглых вырезах находятся фронтальные камеры по 8 Мп с поддержкой фото и видеосъемки в формате 4К. Предустановленные режимы — ночной, портрет, панорама и другие — дают возможность получать фотографии превосходного качества без долгой настройки камеры.</p><h3>Впечатляющая производительность</h3><p>\t Смартфон работает на восьмиядерном процессоре Snapdragon 8+ Gen1 4G при поддержке графического редактора Adreno и процессора искусственного интеллекта Quallcomm 7 поколения.</p><div style=\"text-align: center;\"> <img src=\"https://shop.mts.ru/upload/images/smartfon-huawei-mate-x3-4.jpg\"></div><p>\t Быструю и комфортную работу приложений в режиме многозадачности обеспечивает оперативная память объемом 12 ГБ. Для установки программ и хранения большого количества медиафайлов предусмотрено внутреннее хранилище объемом 512 ГБ.</p><h3>Дизайн, привлекающий внимание</h3><p>\t Смартфон выполнен в лаконичном стиле. На передней панели расположен тонкорамочный экран. На задней — круглый блок камер.</p><div style=\"text-align: center;\"> <img src=\"https://shop.mts.ru/upload/images/smartfon-huawei-mate-x3-5.jpg\"></div><p>\t Шарнир с кулачковым приводом позволяет зафиксировать смартфон в сложенном под 90 градусов состоянии для более комфортного использования. Телефон защищен от попадания влаги по стандарту IPX8 и не боится длительного погружения в пресную воду.</p><div style=\"text-align: center;\"> <img src=\"https://shop.mts.ru/upload/images/smartfon-huawei-mate-x3-6.jpg\"></div><h3>Отличная автономность</h3><p>\t Работу Huawei Mate X3 поддерживает аккумулятор емкостью 4 800 мАч. Благодаря высокой энергоэффективности процессора и экрана одного заряда хватает на весь день.</p><div style=\"text-align: center;\"> <img src=\"https://shop.mts.ru/upload/images/smartfon-huawei-mate-x3-7.jpg\"></div><p>\t Смартфон поддерживает технологию сверхбыстрой зарядки 66 Вт и беспроводной зарядки мощностью до 50 Вт.</p></div> <div class=\"short-characteristic\"></br><h3 class=\"short-characteristic__title\">        Краткие характеристики      </h3> <table class=\"table-specs short-characteristic__table-specs\"><tbody><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Диагональ экрана      </td> <td class=\"table-specs__td\">7.85 \"</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Разрешение экрана      </td> <td class=\"table-specs__td\">2496 x 2224</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Операционная система      </td> <td class=\"table-specs__td\">EMUI 13</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Объем оперативной памяти      </td> <td class=\"table-specs__td\">12 Гб</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Максимальный объем карты памяти      </td> <td class=\"table-specs__td\">256 Гб</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Разрешение основной камеры      </td> <td class=\"table-specs__td\">50 Мп</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Разрешение второй основной камеры      </td> <td class=\"table-specs__td\">13 Мп</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Количество ядер      </td> <td class=\"table-specs__td\">8</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Максимальная частота процессора      </td> <td class=\"table-specs__td\">3.2 ГГц</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Стандарты сотовой связи      </td> <td class=\"table-specs__td\">GSM, 2G, 3G, 4G LTE</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        VoLTE      </td> <td class=\"table-specs__td\">Да</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Беспроводное соединение      </td> <td class=\"table-specs__td\">Bluetooth, NFC, Wi-Fi</td></tr><tr class=\"table-specs__tr\"><td class=\"table-specs__td table-specs__td--name\">        Емкость аккумулятора      </td> <td class=\"table-specs__td\">4800 mAh</td></tr></tbody></table></div>",
                    Price = 149999,
                    Quantity = 55,
                    Category = smartPhoneCategory,
                    Manager = admin,
                    Image = "https://img.mvideo.ru/Big/30067434bb.jpg"
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Клавиатура беспроводная Logitech MX Keys Graphite",
                    Description = "Беспроводная клавиатура Logitech MX Keys Graphite (920-009417) оснащена удобными низкопрофильными клавишами с высокоточным нажатием. На каждой клавише сделано углубление под палец. Модель отличается улучшенным откликом и низким уровнем шума в процессе работы. Клавиатура подключается к компьютеру по Bluetooth или при помощи USB-адаптера. Возможно подключение до трех устройств сразу. Кнопки подсвечиваются (яркость подсветки можно регулировать). При включенном датчике движения подсветка включится при поднесении рук к клавиатуре. На одном заряде модель функционирует в течение 10 дней.>",
                    Price = 11999,
                    Quantity = 25,
                    Category = keyboardCategory,
                    Manager = admin,
                    Image = "https://img.mvideo.ru/Pdb/50174888b.jpg"
                }
            };
            context.Products.AddRange(products);
            context.SaveChanges();
        }
    }
}