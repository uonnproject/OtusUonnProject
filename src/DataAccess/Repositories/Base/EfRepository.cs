using DataAccess.Repositories.Extensions;
using Domain.Abstractions;
using Domain.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories.Base;

public class EfRepository<TEntity>
    : IRepository<TEntity>
    where TEntity : Entity
{
    protected readonly DataContext DataContext;
    private readonly DbSet<TEntity> _entitySet;

    public EfRepository( DataContext dataContext )
    {
        DataContext = dataContext;
        _entitySet = DataContext.Set<TEntity>( );
    }

    public async Task<TEntity> AddAsync(
        TEntity entity,
        CancellationToken cancellationToken )
    {
        if ( entity == null )
            throw new ArgumentNullException( nameof( entity ) );

        var entityEntryAdded = await _entitySet.AddAsync( entity, cancellationToken );

        return entityEntryAdded.Entity;
    }

    public async Task AddRangeAsync(
        IEnumerable<TEntity> entities,
        CancellationToken cancellationToken )
    {
        await _entitySet.AddRangeAsync( entities, cancellationToken );
    }

    public async Task<TEntity?> GetByIdAsync(
        Guid id,
        CancellationToken cancellationToken )
    {
        return await _entitySet.FindAsync( id, cancellationToken );
    }

    public async Task<List<TEntity>> GetByIdsAsync(
        IEnumerable<Guid> ids,
        CancellationToken cancellationToken,
        bool asNoTracking = false )
    {
        return await _entitySet
            .WithNoTracking( asNoTracking )
            .Where( x => ids.Contains( x.Id ) )
            .ToListAsync( cancellationToken );
    }

    public void UpdateAsync(
        TEntity entity,
        CancellationToken cancellationToken )
    {
        DataContext.Entry( entity ).State = EntityState.Modified;
    }

    public void UpdateRangeAsync(
        IEnumerable<TEntity> entities,
        CancellationToken cancellationToken )
    {
        foreach ( var entity in entities )
            DataContext.Entry( entity ).State = EntityState.Modified;
    }

    public void Delete( TEntity entity )
    {
        DataContext.Set<TEntity>( ).Remove( entity );
    }

    public void DeleteRange( IEnumerable<TEntity> entities )
    {
        DataContext.Set<TEntity>( ).RemoveRange( entities );
    }
}