using DataAccess.Abstractions.Pagination;
using Domain.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories.Base;

public static class GenericPaging
{
    public static async Task<List<TEntity>> ToPaginatedListAsync<TEntity>(
        this IQueryable<TEntity> query,
        Pagination pagination,
        CancellationToken cancellationToken,
        bool defaultOrder = false )
        where TEntity : Entity
    {
        pagination.SetupPagination( query );

        if ( pagination.PageSize == 0 )
            throw new ArgumentOutOfRangeException(
                nameof( pagination.PageSize ), "pageSize cannot be zero." );

        if ( defaultOrder )
            query = query.OrderBy( x => x.Id );

        var pageNumZeroStart = pagination.PageNumber == 0
            ? 0
            : pagination.PageNumber - 1;

        if ( pageNumZeroStart != 0 )
            query = query
                .Skip( pageNumZeroStart * pagination.PageSize );

        return await query.Take( pagination.PageSize ).ToListAsync( cancellationToken );
    }
}