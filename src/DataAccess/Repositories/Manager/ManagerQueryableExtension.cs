using DataAccess.Abstractions.Managers;

namespace DataAccess.Repositories.Manager;

public static class ManagerQueryableExtension
{
    public static IQueryable<Domain.Entities.Manager> OrderManagersBy(
        this IQueryable<Domain.Entities.Manager> managers,
        OrderByOptions? orderByOptions )
    {
        if ( orderByOptions == null )
            return managers.OrderByDescending( x => x.Id );

        return orderByOptions switch
        {
            OrderByOptions.SimpleOrder => managers.OrderByDescending( x => x.Id ),
            OrderByOptions.ByBirthDate => managers.OrderByDescending( x => x.BirthDate ),
            OrderByOptions.ByFirstName => managers.OrderByDescending( x => x.FirstName ),
            _ => throw new ArgumentOutOfRangeException( nameof( orderByOptions ), orderByOptions, null )
        };
    }
}