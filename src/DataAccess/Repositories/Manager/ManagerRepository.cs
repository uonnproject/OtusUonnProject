using DataAccess.Abstractions.Managers;
using DataAccess.Repositories.Base;
using DataAccess.Repositories.Extensions;
using Domain.Abstractions;

namespace DataAccess.Repositories.Manager;

public class ManagerRepository
    : EfRepository<Domain.Entities.Manager>, IManagerRepository
{
    public ManagerRepository( DataContext dataContext )
        : base( dataContext )
    {
    }

    public async Task<List<Domain.Entities.Manager>> QueryByFilterAsync(
        SortFilterPageOptions sortFilterPageOptions,
        CancellationToken cancellationToken )
    {
        var predicate =
            FilterBuilders.ManagerFilterPredicateBuilder<Domain.Entities.Manager>.Build( sortFilterPageOptions.Filter );

        return await DataContext.Set<Domain.Entities.Manager>( )
            .WithNoTracking( true )
            .Where( predicate )
            .OrderManagersBy( sortFilterPageOptions.OrderOptions )
            .ToPaginatedListAsync( sortFilterPageOptions.Pagination, cancellationToken );
    }

    public async Task<List<Domain.Entities.Manager>> GetAllAsync(
        SortPageOptions sortPageOptions,
        CancellationToken cancellationToken )
    {
        return await DataContext.Set<Domain.Entities.Manager>( )
            .WithNoTracking( true )
            .OrderManagersBy( sortPageOptions.OrderOptions )
            .ToPaginatedListAsync( sortPageOptions.Pagination, cancellationToken );
    }
}