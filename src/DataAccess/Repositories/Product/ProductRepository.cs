using DataAccess.Abstractions.Products;
using DataAccess.Repositories.Base;
using DataAccess.Repositories.Extensions;
using Domain.Abstractions;
using Domain.Exceptions.Products;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories.Product;

public class ProductRepository
    : EfRepository<Domain.Entities.Product>, IProductRepository
{
    public ProductRepository( DataContext dataContext )
        : base( dataContext )
    {
    }

    public async Task ReduceQuantity(
        Guid productId,
        CancellationToken cancellationToken )
    {
        var productInDb = await DataContext.Set<Domain.Entities.Product>( )
            .FirstOrDefaultAsync( x => x.Id == productId, cancellationToken );

        if ( productInDb == null )
            throw new ProductNotFoundException( productId );

        productInDb.Quantity--;
    }

    public new async Task<Domain.Entities.Product> AddAsync(
        Domain.Entities.Product product,
        CancellationToken cancellationToken )
    {
        var productInDb = await DataContext.Set<Domain.Entities.Product>( )
            .FirstOrDefaultAsync(
                x => x.Name.ToUpper( ).Contains( product.Name.TrimEnd( ).ToUpper( ) ),
                cancellationToken: cancellationToken );

        if ( productInDb != null )
            throw new ProductAlreadyExistsException( product.Id );
        
        // TODO temp solution
        // Guid from seeded user
        product.ManagerId = Guid.Parse( "bd6dd77c-36dc-4c9b-801c-0b531edc9b42" );
        return await base.AddAsync( product, cancellationToken );
    }
    
    public async Task<List<Domain.Entities.Product>> GetAllAsync(
        SortPageOptions sortPageOptions,
        CancellationToken cancellationToken )
    {
        return await DataContext.Set<Domain.Entities.Product>( )
            .WithNoTracking( true )
            .OrderProductsBy( sortPageOptions.OrderOptions )
            .ToPaginatedListAsync( sortPageOptions.Pagination, cancellationToken );
    }    
}