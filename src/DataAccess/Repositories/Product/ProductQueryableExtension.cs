using DataAccess.Abstractions.Products;

namespace DataAccess.Repositories.Product;

public static class ProductQueryableExtension
{
    public static IQueryable<Domain.Entities.Product> OrderProductsBy(
        this IQueryable<Domain.Entities.Product> products,
        OrderByOptions? orderByOptions )
    {
        if ( orderByOptions == null )
            return products.OrderByDescending( x => x.Id );

        return orderByOptions switch
        {
            OrderByOptions.SimpleOrder => products.OrderByDescending( x => x.Id ),
            OrderByOptions.ByName => products.OrderByDescending( x => x.Name ),
            OrderByOptions.ByCategory => products.OrderByDescending( x => x.CategoryId ),
            _ => throw new ArgumentOutOfRangeException( nameof( orderByOptions ), orderByOptions, null )
        };
    }    
}