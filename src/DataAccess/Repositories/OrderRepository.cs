using Domain.Abstractions;
using Domain.Entities;

namespace DataAccess.Repositories;

public class OrderRepository : IOrderRepository
{
    private readonly DataContext _dataContext;

    public OrderRepository(DataContext dataContext)
    {
        _dataContext = dataContext;
    }

    public async Task CreateAsync(Order order, CancellationToken cancellationToken)
    {
        await _dataContext.Set<Order>().AddAsync(order, cancellationToken);
    }
}