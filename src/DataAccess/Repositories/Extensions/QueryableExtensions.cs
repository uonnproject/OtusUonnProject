using Domain.Entities.Base;
using LinqKit;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories.Extensions;

public static class QueryableExtensions
{
    public static IQueryable<TEntity> WithNoTracking<TEntity>(
        this IQueryable<TEntity> @this,
        bool asNoTracking ) where TEntity : Entity
    {
        return asNoTracking ? @this.AsNoTracking( ) : @this;
    }

    public static IQueryable<TEntity> ApplyFilter<TEntity>(
        this IQueryable<TEntity> @this,
        ExpressionStarter<TEntity> predicate )
    {
        return @this.Where( predicate );
    }
}